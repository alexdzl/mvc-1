﻿using System.ComponentModel.DataAnnotations;
using MvcForms.Infrastructure;

namespace MvcForms.Models
{
    public class PersonInfoViewModel
    {
        [Display(Name = "Есть судимость")]
        [Required]
        public bool HaveCriminalRecord { set; get; }
        [Display(Name = "Номер статьи")]
        [RequiredIf(nameof(HaveCriminalRecord), true, ErrorMessage = "\"Номер статьи\" должно быть заполнено")]
        public string ArticleNumber { set; get; }
    }
}