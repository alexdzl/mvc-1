﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MvcForms.Models;

namespace MvcForms.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Index(PersonInfoViewModel personInfoViewModel)
        {
            if (ModelState.IsValid)
            {
                return RedirectToAction("Done");
            }
            return View(personInfoViewModel);
        }

        public ActionResult Done()
        {
            return View();
        }
    }
}