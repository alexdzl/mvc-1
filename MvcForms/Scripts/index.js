﻿function FormCtrl(name) {
    window[name] = this;
    var self = this;
    toggleControls(false);

    self.onRadioClick = function () {
        toggleControls(this.value === "True");
    };

    $(":radio").click(self.onRadioClick);

    function toggleControls(show) {
        $(".article-number").toggle(show);
    };
};

var form1 = new FormCtrl("form1");
